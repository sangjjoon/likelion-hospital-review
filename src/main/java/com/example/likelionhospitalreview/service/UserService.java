package com.example.likelionhospitalreview.service;

import com.example.likelionhospitalreview.domain.dto.UserJoinReq;
import com.example.likelionhospitalreview.domain.dto.UserJoinRes;
import com.example.likelionhospitalreview.domain.entity.User;
import com.example.likelionhospitalreview.exception.ErrorCode;
import com.example.likelionhospitalreview.exception.HospitalReviewAppException;
import com.example.likelionhospitalreview.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;


    public UserJoinRes join(UserJoinReq userJoinReq) {
        userRepository.findByUserName(userJoinReq.getUserName())
                .ifPresent(user -> {
                    throw new HospitalReviewAppException(ErrorCode.DUPLICATED_USER_NAME, String.format("%s는 이미 존재하는 회원입니다.", userJoinReq.getUserName()));
                });
        User savedUser = userRepository.save(userJoinReq.toEntity());
        return UserJoinRes.builder()
                .userName(savedUser.getUserName())
                .build();
    }
}
