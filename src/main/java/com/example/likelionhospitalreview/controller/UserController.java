package com.example.likelionhospitalreview.controller;

import com.example.likelionhospitalreview.domain.dto.UserJoinReq;
import com.example.likelionhospitalreview.domain.dto.UserJoinRes;
import com.example.likelionhospitalreview.domain.entity.Response;
import com.example.likelionhospitalreview.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;

    @PostMapping("/join")
    public Response<UserJoinRes> join(@RequestBody UserJoinReq userJoinReq) {
        log.debug("join 실행");
        log.info("userName : {}", userJoinReq.getUserName());
        UserJoinRes user = userService.join(userJoinReq);
        return Response.success(new UserJoinRes(user.getUserName()));
    }
}
